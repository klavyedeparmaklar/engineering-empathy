import React, { useState, useEffect, useRef } from 'react'

// Face Api 
import * as faceapi from 'face-api.js'

// App Css
import './App.css';

function App() {
  const videoHeight = 480
  const videoWidth = 640
  const [initializing, setInitializing] = useState(false)
  const videoRef = useRef()
  const canvasRef = useRef()
  const canvasRef2 = useRef()

  useEffect(() => {
    const loadModals = async () => {
      const MODEL_URL = process.env.PUBLIC_URL + '/models'
      setInitializing(true)
      Promise.all([
        faceapi.nets.tinyFaceDetector.loadFromUri(MODEL_URL),
        faceapi.nets.faceLandmark68Net.loadFromUri(MODEL_URL),
        faceapi.nets.faceRecognitionNet.loadFromUri(MODEL_URL),
        faceapi.nets.faceExpressionNet.loadFromUri(MODEL_URL),
      ]).then(startVideo)
    }
    loadModals()
  }, [])

  const startVideo = () => {
    navigator.getUserMedia(
      {
        video: {},
      },
      (stream) => (videoRef.current.srcObject = stream),
      (err) => console.log(err)
    );
  };

  const handleVideoOnPlay = () => {
    setInterval(async () => {
      if (initializing) {
        setInitializing(false)
      }

      canvasRef.current.innerHTML = faceapi.createCanvasFromMedia(videoRef.current)
      const displaySize = {
        width: videoWidth,
        height: videoHeight
      }
      faceapi.matchDimensions(canvasRef.current, displaySize)
      const detections = await faceapi.detectAllFaces(videoRef.current, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
      const resizedDetections = faceapi.resizeResults(detections, displaySize)
      canvasRef.current.getContext('2d').clearRect(0, 0, videoWidth, videoHeight)
      faceapi.draw.drawDetections(canvasRef.current, resizedDetections)
      faceapi.draw.drawFaceLandmarks(canvasRef.current, resizedDetections)
      faceapi.draw.drawFaceExpressions(canvasRef.current, resizedDetections)
    }, 1000)
  }

  // const captureSnapshot = () => {
  //   var ctx = canvasRef2.current.getContext("2d");
  //   var img = new Image();
  //   ctx.drawImage(videoRef.current, 0, 0, videoWidth, videoHeight);
  //   img.src = canvasRef2.current.toDataURL("image");
  //   img.width = videoWidth;
  //   img.height = videoHeight;
  // };

  return (
    <div className="App">
      <span>{initializing ? 'Initializing' : 'Ready'}</span>
      <div className="d-flex justify-content-center">
        <video ref={videoRef} autoPlay muted height={videoHeight} width={videoWidth} onPlay={handleVideoOnPlay} />
        <canvas className="position-absolute" ref={canvasRef} />
      </div>
    </div>
  );
}

export default App;
